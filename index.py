import urllib.request, json, datetime
import codecs

urls = {
    'https://www.kiwime.com/oqdb/files/3236295263/OpenQuizzDB_236/openquizzdb_236.json',
}

timestamp = datetime.datetime.utcnow().timestamp()
# question_file = open("quizz-questions" + str(timestamp) + ".lua", "w+")
# theme_file = open("quizz-themes.lua", "a+")
theme_file = codecs.open("quizz-themes.lua", "a+", "utf-8")
theme_index_file = open("themeid.txt", "r")
theme_index = theme_index_file.readline()
question_file = codecs.open("./QuizzFiles/quizz-questions" + str(timestamp) + "---" + theme_index + ".lua", "w+", "utf-8")

qi = 1

level = 'débutant'
# level = 'confirmé'
# level = 'expert'

for url in urls:
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    theme_index = int(theme_index) + 1
    theme_file.write("themes[\"" + str(theme_index) + "\"] = \"" + data['thème'] + "\"\r")
    question_file.write("quizz_questions[\"" + str(theme_index) + "\"] = {}\r")
    for index in data['quizz']['fr'][level]:
        question_file.write("quizz_questions[\"" + str(theme_index) + "\"][\"q" + str(qi) + "\"] = \"" + index['question'] + "\"\r")
        question_file.write("quizz_questions[\"" + str(theme_index) + "\"][\"a" + str(qi) + "\"] = \"" + index['réponse'] + "\"\r")
        qi = qi + 1

question_file.close()
theme_file.close()
theme_index_file.close()

theme_index_file = open("themeid.txt", "w+")
theme_index_file.write(str(theme_index))
theme_index_file.close()